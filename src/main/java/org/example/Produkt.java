package org.example;

public class Produkt {

    private int id;
    private String nazwa;
    private Double cena;

    private int ilosc;

    public int getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Double getCena() {
        return cena;
    }

    public int getIlosc() { return ilosc; }

    public Produkt(int id, String nazwa, Double cena, int ilosc) {
        this.id = id;
        this.nazwa = nazwa;
        this.cena = cena;
        this.ilosc = ilosc;
    }

    public Produkt(int id, String nazwa, Double cena) {
        this.id=id;
        this.nazwa=nazwa;
        this.cena=cena;
    }




}

package org.example;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.awt.Desktop;
import java.io.IOException;

import static org.example.MenuController.*;
import static org.example.SampleController.*;


@Component
public class PDFDocument{

    public PDFDocument(String cena) {
        try {
            int aux = 17;
            int height = 180 + listaProduktow.size() * aux;
            Rectangle pageSize = new Rectangle(300, height);
            Document document = new Document(pageSize);
            document.setMargins(5,4,4,4);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("SavedPDFs\\receipt.pdf"));
            document.open();
            document.add(new Paragraph("Imie: " + imieString));
            document.add(new Paragraph("Nazwisko: " + nazwiskoString));
            document.add(new Paragraph("Miasto: " + miastoString));
            document.add(new Paragraph("Ulica: " + ulicaString));
            document.add(new Paragraph("Nr domu/lokalu: " + nr_domuString));
            document.add(new Paragraph("___________________________________________"));
            document.add(new Paragraph(" "));

            PdfPTable table = new PdfPTable(3);
            table.setWidths(new int[]{3, 1, 2});
            table.addCell(createCell("Produkt", 0, Element.ALIGN_LEFT));
            table.addCell(createCell("Ilosc", 0, Element.ALIGN_RIGHT));
            table.addCell(createCell("Cena", 0, Element.ALIGN_RIGHT));

            for(int i = 0 ; i < nazwyProduktow.size() ; i++) {
                table.addCell(createCell(nazwyProduktow.get(i), 0, Element.ALIGN_LEFT));
                table.addCell(createCell(listaIlosci.get(i), 0, Element.ALIGN_RIGHT));
                table.addCell(createCell(ceny.get(i).toString(), 0, Element.ALIGN_RIGHT));
            }
            document.add(table);
            Paragraph paragraph = new Paragraph("Suma: " + cena + "  ");
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragraph);
            document.close();

            File file = new File("SavedPDFs\\receipt.pdf");

            Desktop desktop = Desktop.getDesktop();
            if(file.exists()){
                desktop.open(file);
            }

        } catch (DocumentException | FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PdfPCell createCell(String content, float borderWidth, int alignment) {
        final String FONT = "static/arial.ttf";

        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);

        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setBorderWidth(borderWidth);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }
}

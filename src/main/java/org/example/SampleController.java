package org.example;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.json.JSONException;


import java.io.IOException;

public class SampleController {

    @FXML
    private TextField imie;
    @FXML
    private TextField nazwisko;
    @FXML
    private TextField miasto;
    @FXML
    private TextField ulica;
    @FXML
    private TextField nr_domu;
    @FXML
    private Button przejdzDoMenu;
    @FXML
    private Label error;

    static String imieString = "";
    static String nazwiskoString = "";
    static String miastoString = "";
    static String ulicaString = "";
    static String nr_domuString = "";


    public void initialize(){
        if(!(imieString.isEmpty()) || !(nazwiskoString.isEmpty()) ||  !(miastoString.isEmpty())
                || !(ulicaString.isEmpty()) || !(nr_domuString.isEmpty())){

            imie.setText(imieString);
            nazwisko.setText(nazwiskoString);
            miasto.setText(miastoString);
            ulica.setText(ulicaString);
            nr_domu.setText(nr_domuString);
        }
    }

    private boolean sprawdzDane(){
        if(imie.getText().trim().isEmpty() || nazwisko.getText().trim().isEmpty() || miasto.getText().trim().isEmpty()
            || ulica.getText().trim().isEmpty() || nr_domu.getText().trim().isEmpty()){
            error.setText("Nie podano wszystkich wymaganych pól");
            return false;
        }

        else{
            error.setText("");

            imieString = imie.getText();
            nazwiskoString = nazwisko.getText();
            miastoString = miasto.getText();
            ulicaString = ulica.getText();
            nr_domuString = nr_domu.getText();

            return true;
        }
    }

    @FXML
    private void przejdzDoMenu() throws IOException {
        if (sprawdzDane()) {
            App.setRoot("menu");
        }
    }
}

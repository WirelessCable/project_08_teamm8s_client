package org.example;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;

import static java.lang.Math.round;
import static org.example.MenuController.*;

public class KoszykController {

    @FXML
    private Label koszyk;
    @FXML
    private Label aktywnePromocje;
    @FXML
    private TextField kwotaZamowieniaText;
    @FXML
    private Label kwotaZamowienia;
    @FXML
    private TableView tabelaKoszyk;
    @FXML
    private Label wyswietlAktywnePromocje;
    @FXML
    private Button wyczyscKoszyk;
    @FXML
    private Button wrocDoKoszyka;
    @FXML
    private Button zlozZamowienie;

    @FXML
    private TableColumn<?, ?> lp;

    @FXML
    private TableColumn<?, ?> nazwa;

    @FXML
    private TableColumn<?, ?> cenaZTabeli;

    @FXML
    private TableColumn<?, ?> ilosc;

    @FXML
    private void przejdzDoMenu() throws IOException {
        App.setRoot("menu");
    }

    @FXML
    private void wyswietlListe(){
        System.out.println(listaProduktow.toString());
        System.out.println(listaIlosci.toString());
//        System.out.println(nazwyProduktow.toString());
//        System.out.println(ceny.toString());
    }

    @FXML
    private void wyswietlZamowienie(){
        lp.setCellValueFactory(new PropertyValueFactory<>("id"));
        nazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        cenaZTabeli.setCellValueFactory(new PropertyValueFactory<>("cena"));
        ilosc.setCellValueFactory(new PropertyValueFactory<>("ilosc"));
        for(int i = 0; i < listaProduktow.size(); i++){
            tabelaKoszyk.getItems().add(new Produkt(i + 1, nazwyProduktow.get(i), ceny.get(i)* Double.parseDouble(listaIlosci.get(i)), Integer.parseInt(listaIlosci.get(i))));
        }
    }

    @FXML
    private void initialize(){
        if(listaProduktow.isEmpty())
            zlozZamowienie.setDisable(true);
        wyswietlZamowienie();
        kwotaZamowienia();
    }

    @FXML
    private void wyczyscKoszyk(){
        listaIlosci.clear();
        ceny.clear();
        listaProduktow.clear();
        nazwyProduktow.clear();
        tabelaKoszyk.getItems().clear();
        kwotaZamowieniaText.setText(" ");
        wyswietlAktywnePromocje.setText("");
        zlozZamowienie.setDisable(true);
    }

    @FXML
    private void kwotaZamowienia(){
        String text = "";
        Double kwota = 0.0;
        for(int i = 0; i < listaIlosci.size(); i++){
            kwota += ceny.get(i)* Double.parseDouble(listaIlosci.get(i));
        }

        if(listaProduktow.size()>2){
            int licznik=0;
            for(String i:listaProduktow){
                if(Integer.parseInt(i)<11){
                    licznik++;
                }
            }
            if(licznik>=2){
                if (listaProduktow.contains("11")) {
                    kwota-=6;
                    text += "Darmowy napój\n";
                }
                else if(listaProduktow.contains("12")){
                    kwota-=5;
                    text += "Darmowy napój\n";
                }

            }
            else if(licznik==1){
                for(int i=0;i<listaProduktow.size();i++){
                   if(!(listaProduktow.get(i).equals("11")) || !(listaProduktow.get(i).equals("12"))){
                       if(Integer.parseInt(listaIlosci.get(i))>=2){
                           if (listaProduktow.contains("11")) {
                               kwota-=6;
                               text += "Darmowy napój\n";
                           }
                           else if(listaProduktow.contains("12")){
                               kwota-=5;
                               text += "Darmowy napój\n";
                           }

                       }
                   }
                }

            }

        }
        else if(listaProduktow.size()==2){
            int licznik=0;
            for(String i:listaProduktow){
                if(Integer.parseInt(i)<11){
                    licznik++;
                }
            }
            if(licznik==1){
                for(int i=0;i<listaProduktow.size();i++){
                    if(!(listaProduktow.get(i).equals("11")) || !(listaProduktow.get(i).equals("12"))){
                        if(Integer.parseInt(listaIlosci.get(i))>=2){
                            if (listaProduktow.contains("11")) {
                                kwota-=6;
                                text += "Darmowy napój\n";
                            }
                            else if(listaProduktow.contains("12")){
                                kwota-=5;
                                text += "Darmowy napój\n";
                            }
                        }
                    }
                }
            }
        }


        if(kwota>100){
           kwota = kwota*0.8;
           text += "Zakup tanszy o 20%";
        }


        double kw = Math.round(kwota*100.0)/100.0;
        wyswietlAktywnePromocje.setText(text);
        kwotaZamowieniaText.setText(kw + " PLN");
    }

    @FXML
    public void zlozZamowienie() throws IOException {
        PDFDocument pdfDocument = new PDFDocument(kwotaZamowieniaText.getText());
    }

}

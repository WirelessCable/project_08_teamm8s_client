package org.example;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.TextAlignment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MenuController implements EventHandler<Event> {

    @FXML
    private Label menu;

    @FXML
    private Label pizza50na50;

    @FXML
    private Label cena;

    @FXML
    private TextField ilosc1;
    @FXML
    private TextField ilosc2;
    @FXML
    private TextField ilosc3;
    @FXML
    private TextField ilosc4;
    @FXML
    private TextField ilosc5;
    @FXML
    private TextField ilosc6;
    @FXML
    private TextField ilosc7;
    @FXML
    private TextField ilosc8;
    @FXML
    private TextField ilosc9;
    @FXML
    private TextField ilosc10;
    @FXML
    private TextField ilosc11;
    @FXML
    private TextField ilosc12;



    @FXML
    private Button dodaj;
    @FXML
    private Button poprawDaneDostawy;
    @FXML
    private Button przejdzDoKoszyka;
    @FXML
    private Button dodaj1;
    @FXML
    private Button dodaj2;
    @FXML
    private Button dodaj3;
    @FXML
    private Button dodaj4;
    @FXML
    private Button dodaj5;
    @FXML
    private Button dodaj6;
    @FXML
    private Button dodaj7;
    @FXML
    private Button dodaj8;
    @FXML
    private Button dodaj9;
    @FXML
    private Button dodaj10;
    @FXML
    private Button dodaj11;
    @FXML
    private Button dodaj12;
    @FXML
    private ChoiceBox pizza1;

    @FXML
    private ChoiceBox pizza2;

    @FXML
    private TableView tabelaKoszyk;

    @FXML
    private TableColumn<?, ?> lp;

    @FXML
    private TableColumn<?, ?> nazwa;

    @FXML
    private TableColumn<?, ?> cenaZTabeli;

    @FXML
    private Label error;

    @FXML
    private Label dostepnePromocje;


    static ArrayList<String> listaProduktow = new ArrayList<String>();
    static ArrayList<String> listaIlosci = new ArrayList<String>();

    static ArrayList<String> nazwyProduktow = new ArrayList<String>();
    static ArrayList<Double> ceny = new ArrayList<Double>();

    public MenuController() {
    }

    @FXML
    private void przejdzDoDanych() throws IOException {
        App.setRoot("sample");
    }

    @FXML
    private void przejdzDoKoszyka() throws IOException {
        App.setRoot("koszyk");
//        System.out.println(listaProduktow.toString());
//        System.out.println(listaIlosci.toString());
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONArray json = new JSONArray(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    @FXML
    private void wyswietlMenu() throws IOException, JSONException {

        JSONArray json = readJsonFromUrl("http://localhost:8080/menu");

        lp.setCellValueFactory(new PropertyValueFactory<>("id"));
        nazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        cenaZTabeli.setCellValueFactory(new PropertyValueFactory<>("cena"));
        for (int i = 0; i < 12; i++) {
            JSONObject jsonObject = json.getJSONObject(i);
            tabelaKoszyk.getItems().add(new Produkt(((int) jsonObject.get("id")), jsonObject.get("nazwa").toString(), (Double) jsonObject.get("cena")));
        }

        for (int i = 0; i < 10; i++) {
            JSONObject jsonObject = json.getJSONObject(i);
            pizza1.getItems().add(jsonObject.get("nazwa").toString());
            pizza2.getItems().add(jsonObject.get("nazwa").toString());
        }

    }

    private void cenaPizz50na50() throws IOException, JSONException {
        String nazwa1 = pizza1.getValue().toString();
        String nazwa2 = pizza2.getValue().toString();
        JSONArray json = readJsonFromUrl("http://localhost:8080/menu");

        lp.setCellValueFactory(new PropertyValueFactory<>("id"));
        nazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        cenaZTabeli.setCellValueFactory(new PropertyValueFactory<>("cena"));


        if (nazwa1.equals(nazwa2)){
            error.setText("Wybrane pizze są takie same!");
            dodaj.setDisable(true);
            cena.setText("");
        }
        else {
            Double kwota = 0.0;
            for (int i = 0; i < 10; i++) {
                if (nazwa1.equals(json.getJSONObject(i).get("nazwa").toString())) {
                    kwota += Double.parseDouble(json.getJSONObject(i).get("cena").toString());
                }
                if (nazwa2.equals(json.getJSONObject(i).get("nazwa").toString())) {
                    kwota += Double.parseDouble(json.getJSONObject(i).get("cena").toString());
                }
            }
            dodaj.setDisable(false);
            kwota /= 2;

            cena.setText(String.valueOf(kwota) + " zł ");
            error.setText("");
        }
    }

    public void sprawdzButton() throws IOException, JSONException {

        cenaPizz50na50();

    }

    public void dodaj50(){

        listaProduktow.add("-1");
        listaIlosci.add("1");
        nazwyProduktow.add(pizza1.getValue().toString() +" / "+ pizza2.getValue().toString());
        ceny.add(Double.valueOf(cena.getText().substring(0,cena.getText().length()-4)));

    }

    // RUN WHEN START-UP
    public void initialize() throws IOException, JSONException {
        cena.setAlignment(Pos.CENTER_RIGHT);
        cena.setStyle("-fx-border-color: black;");
        wyswietlMenu();
        dodaj.setDisable(true);
        dostepnePromocje.setText("Dostępne promocje:\n" +
                "- pizza 50 na 50\n" +
                "- przy zakupie 2 pizz napój za darmo\n" +
                "- przy zakupie powyżej 100 zł 20% rabatu");
    }


    // GET ID OF PRESSED BUTTON
    @Override
    public void handle(Event event) {
        String pressedButton = ((Control)event.getSource()).getId();

        String identificator;
        if(pressedButton.length() == 6){
            identificator = pressedButton.substring(pressedButton.length() - 1);
        }else{
            identificator = pressedButton.substring(pressedButton.length() - 2);
        }

        try {
            JSONArray json = readJsonFromUrl("http://localhost:8080/menu");
            lp.setCellValueFactory(new PropertyValueFactory<>("id"));
            nazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
            cenaZTabeli.setCellValueFactory(new PropertyValueFactory<>("cena"));
            JSONObject jsonObject;

            switch (identificator){
                case "1":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc1.getText());
                    jsonObject = json.getJSONObject(0);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "2":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc2.getText());
                    jsonObject = json.getJSONObject(1);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "3":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc3.getText());
                    jsonObject = json.getJSONObject(2);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "4":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc4.getText());
                    jsonObject = json.getJSONObject(3);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "5":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc5.getText());
                    jsonObject = json.getJSONObject(4);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "6":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc6.getText());
                    jsonObject = json.getJSONObject(5);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "7":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc7.getText());
                    jsonObject = json.getJSONObject(6);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "8":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc8.getText());
                    jsonObject = json.getJSONObject(7);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "9":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc9.getText());
                    jsonObject = json.getJSONObject(8);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "10":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc10.getText());
                    jsonObject = json.getJSONObject(9);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "11":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc11.getText());
                    jsonObject = json.getJSONObject(10);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
                case "12":
                    listaProduktow.add(identificator);
                    listaIlosci.add(ilosc12.getText());
                    jsonObject = json.getJSONObject(11);
                    nazwyProduktow.add((String) jsonObject.get("nazwa"));
                    ceny.add((Double) jsonObject.get("cena"));
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

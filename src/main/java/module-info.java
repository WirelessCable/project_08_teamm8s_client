module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.datatransfer;
    requires android.json;
    requires pdfbox;
    requires itext;
    requires itextpdf;
    requires org.slf4j;
    requires spring.context;
    requires java.desktop;

    opens org.example to javafx.fxml;
    exports org.example;
}